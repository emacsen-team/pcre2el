Source: pcre2el
Section: lisp
Priority: optional
Maintainer: Debian Emacsen team <debian-emacsen@lists.debian.org>
Uploaders: Lev Lamberov <dogsleg@debian.org>
Build-Depends: debhelper-compat (= 13),
               dh-elpa,
               elpa-a
Standards-Version: 4.6.2
Rules-Requires-Root: no
Testsuite: autopkgtest-pkg-elpa
Homepage: https://github.com/joddie/pcre2el
Vcs-Browser: https://salsa.debian.org/emacsen-team/pcre2el
Vcs-Git: https://salsa.debian.org/emacsen-team/pcre2el.git

Package: elpa-pcre2el
Architecture: all
Depends: ${elpa:Depends},
         ${misc:Depends},
         elpa-a
Recommends: emacs (>= 46.0)
Enhances: emacs
Description: Emacs mode to convert between PCRE, Emacs and rx regexp syntax
 This package provides `pcre2el' or `rxt' (RegeXp Translator or RegeXp
 Tools), a utility for working with regular expressions in Emacs,
 based on a recursive-descent parser for regexp syntax. In addition to
 converting (a subset of) PCRE syntax into its Emacs equivalent, it
 can do the following:
 .
  - convert Emacs syntax to PCRE;
  - convert either syntax to `rx', an S-expression based regexp syntax
    untangle complex regexps by;
  - showing the parse tree in `rx' form and highlighting the;
  - corresponding chunks of code show the complete list of strings
    (productions) matching a regexp, provided the list is finite;
  - provide live font-locking of regexp syntax (so far only for Elisp
    buffers).
